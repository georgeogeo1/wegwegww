class CreatePostIds < ActiveRecord::Migration
  def change
    create_table :post_ids do |t|
      t.integer :title
      t.text :body

      t.timestamps null: false
    end
  end
end
